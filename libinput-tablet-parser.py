#!/usr/bin/env python3

import sys, subprocess, shlex

CHECKED_EVENTS = {
    'TABLET_PAD_BUTTON',
}

XDO_ETYPES = {
    'pressed':  'keydown',
    'released': 'keyup',
}

KEYS = {
    '0': 'comma',
    '1': 'period',
    '2': 'colon',
    '3': 'ctrl+Delete',
}

def run(command):
    """Runs a shell command."""
    subprocess.Popen(shlex.split(command))

def setup(device=None):
    """Starts a libinput-debug-events process for listening to devices."""
    devicestr = f' --device {device}' if device else ''
    command = f'stdbuf -oL -- libinput-debug-events{devicestr}'
    process = subprocess.Popen(shlex.split(command),
                               stdout=subprocess.PIPE,
                               bufsize=0,
                               universal_newlines=True)
    return process

def loop(process):
    """Starts the main event loop for parsing device signals."""
    for line in process.stdout:
        dev, event, time, *data = line.split()
        if event not in CHECKED_EVENTS:
            continue

        if event == 'TABLET_PAD_BUTTON':
            button, etype, *_ = data
            try:
                xdo_etype = XDO_ETYPES[etype]
                key = KEYS[button]
            except KeyError:
                continue

            run(f'xdotool {xdo_etype} {key}')

if __name__ == '__main__':
    device = sys.argv[1] if len(sys.argv) > 1 else None
    process = setup(device)
    loop(process)
