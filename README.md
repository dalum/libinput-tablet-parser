# Overview

libinput-tablet-parser is a simple Python script for parsing libinput tablet
button presses and translating them into X11 key presses.  The script is
intended as a temporary fix, until Gnome and similar projects implement proper
tablet button press handling for libinput.

# Dependencies

* libinput (libinput-debug-events)
* xdotool

# Usage

To run the script, you must be a member of the `input` group:

    sudo gpasswd -a $USER input
  
You must restart your session (login and out) in order for the change to take
effect.

Make the script executable:

    chmod +x libinput-tablet-parser.py
  
And run:
  
    ./libinput-tablet-parser.py
  
This will launch libinput-tablet-parser which will listen for tablet button
press events.  To limit overhead, you may want to specify a particular input
device (/dev/input/eventX):

    ./libinput-tablet-parser.py /dev/input/eventX
  
which will tell libinput-debug-events to only listen for events on that device.

# Configuration

The script itself is quite short and simple, and intended to be modified and
extended to suit the needs of the user.  Configuration is therefore to be done
manually by opening the script in your favourite editor and changing the
variables as you see fit.